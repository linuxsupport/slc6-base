# Scientific Linux CERN 6 Docker images

http://cern.ch/linux/scientific6/

## What is Scientific Linux CERN 6 (SLC6)

Scientific Linux CERN 6 is a Linux distribution build within the framework of Scientific Linux which in turn is rebuilt from the freely available Red Hat Enterprise Linux 6 (Server) product sources under terms and conditions of the Red Hat EULA. Scientific Linux CERN is built to integrate into the CERN computing environment but it is not a site-specific product: all CERN site customizations are optional and can be deactivated for external users.

## Current release: SLC 6.8

Scientific Linux CERN 6.9 is the current minor release of SLC6

### Image building

```
koji image-build slc6-base 6.x.`date "+%Y%m%d"` slc6-image-6x http://linuxsoft.cern.ch/cern/slc69/x86_64 x86_64 \
     --ksurl=git+ssh://git@gitlab.cern.ch:7999/linuxsupport/slc6-base#master \
     --kickstart=slc6-base-docker.ks  --distro RHEL-6.9 --format docker --ksversion RHEL7 --factory-parameter dockerversion 1.10.1 \
	 --factory-parameter=docker_env '["PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"]' \
	 --factory-parameter=docker_cmd '["/bin/bash"]' \
	 --scratch
```

note: ksversion RHEL7 <- otherwise kickstart %packages --nocore not acccepted.

### Latest image
```20190731: https://koji.cern.ch/taskinfo?taskID=1506116```

```20190522: http://koji.cern.ch/koji/taskinfo?taskID=1453374``` (Add default PATH and default run CMD)

```20180622: http://koji.cern.ch/koji/taskinfo?taskID=1162172``` (SLC6.10)

```20180516: http://koji.cern.ch/koji/taskinfo?taskID=1127541```

```20180316: http://koji.cern.ch/koji/taskinfo?taskID=1075027```

```20180112:http://koji.cern.ch/koji/taskinfo?taskID=1020940```

```20171114: http://koji.cern.ch/koji/taskinfo?taskID=973569```

```20170920: http://koji.cern.ch/koji/taskinfo?taskID=928005```

