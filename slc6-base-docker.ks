#
# CERN SLC6X, based on CentOS Cloud SIG , building on tag slc6x-image-6x
#
install
text
keyboard us
lang en_US.UTF-8
timezone --utc Europe/Zurich
network --onboot yes --device eth0 --bootproto dhcp --ipv6 auto
rootpw --iscrypted $THIS-IS-NOT-A-PASSWORD$
authconfig --enableshadow --passalgo=sha512
selinux --disabled
firewall --disabled
repo --name="SLC6X" --baseurl="http://linuxsoft.cern.ch/cern/slc69/x86_64/"
repo --name="SLC6X updates" --baseurl="http://linuxsoft.cern.ch/cern/slc69/x86_64/yum/updates/"
repo --name="SLC6X extras" --baseurl="http://linuxsoft.cern.ch/cern/slc69/x86_64/yum/extras/"

zerombr
clearpart --all --initlabel
part / --fstype ext4 --size=1024 --grow
reboot

%packages  --excludedocs --nobase --nocore
bash
bind-utils
CERN-CA-certs
grub
hepix
iproute
iputils
openldap-clients
cern-wrappers
krb5-workstation
cyrus-sasl-gssapi
passwd
rootfiles
sl-release
shadow-utils
vim-minimal
yum
yum-plugin-ovl
yum-autoupdate
-yum-firstboot
-*-firmware
-kernel-firmware

%end

%pre --log=/root/anaconda-pre.log
sed -i 's/^repo /repo --noverifyssl /' /tmp/ks.cfg
%end

%post
# randomize root password and lock root account
dd if=/dev/urandom count=50 | md5sum | passwd --stdin root
passwd -l root

# create necessary devices
/sbin/MAKEDEV /dev/console

rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-cern
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-6

# cleanup unneeded stuff
rpm -e kernel | tee  /root/yum-removal.log

yum -y remove kernel-firmware \
       dash b43-openfwwf device-mapper-libs \
       dracut* e2fsprogs* efibootmgr grubby \
       libpciaccess dhclient dhcp-common \
       pciutils-libs redhat-logos passwd \
       kbd-misc acl sudo tar plymouth* which \
       m4 snappy bc busybox mysql-libs lzo libcap-ng \
       fipscheck-lib file elfutils-libs bzip2 attr policycoreutils \
       passwd| tee -a /root/yum-removal.log

# kernel uninstall fails for 2.6.32-642.11.1.el6
rpm -e kernel --noscripts

# unused
rm -rf /boot


# Keep yum from installing documentation. It takes up too much space.
sed -i '/keepcache=0/a tsflags=nodocs' /etc/yum.conf

# Remove files that are known to take up lots of space but leave
# directories intact since those may be required by new rpms.

#Generate installtime file record
/bin/date +%Y%m%d_%H%M > /etc/BUILDTIME

# locales
rm -f /usr/lib/locale/locale-archive
localedef -v -c -i en_US -f UTF-8 en_US.UTF-8

# remove docs/man
find /usr/share/{man,doc,info,gnome/help} -type f | xargs /bin/rm

#  sln
rm -f /sbin/sln

#  ldconfig
rm -rf /etc/ld.so.cache
rm -rf /var/cache/ldconfig/*

# Clean up after yum
rm -rf /var/lib/yum/*
rm -rf /var/cache/yum/*

# Selinux isn't installed. remove unowned files
rm -rf /etc/selinux/targeted/*

# Clean up after install
rm -rf /var/log/*
rm -rf /etc/dhcp/*


############# CERN'ify ########################################################


cat > /etc/krb5.conf <<EOF
[libdefaults]
 default_realm = CERN.CH
 ticket_lifetime = 25h
 renew_lifetime = 120h
 forwardable = true
 proxiable = true
 default_tkt_enctypes = arcfour-hmac-md5 aes256-cts aes128-cts des3-cbc-sha1 des-cbc-md5 des-cbc-crc
 allow_weak_crypto = true
 chpw_prompt = true

[realms]
 CERN.CH = {
  default_domain = cern.ch
  kpasswd_server = cerndc.cern.ch
  admin_server = cerndc.cern.ch
  kdc = cerndc.cern.ch
  }

[domain_realm]
 .cern.ch = CERN.CH

pam = {
   external = true
   krb4_convert =  false
   krb4_convert_524 =  false
   krb4_use_as_req =  false
   ticket_lifetime = 25h
   use_shmem = sshd
 }

EOF

cat > /etc/openldap/ldap.conf <<EOF
#
# LDAP CERN Defaults
#

# See ldap.conf(5) for details
# This file should be world readable but not world writable.

#BASE DC=cern,DC=ch
#note cerndc provides gssapi auth, xldap does not.
#HOST cerndc.cern.ch  # or xldap.cern.ch
#SIZELIMIT 12
#DEREF always

TLS_CACERTDIR /etc/openldap/certs
TLS_REQCERT demand
SSL start_tls

# Turning this off breaks GSSAPI used with krb5 when rdns = false
SASL_NOCANON	on

EOF

%end
